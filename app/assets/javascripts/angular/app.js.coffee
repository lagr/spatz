app = angular.module 'spatz', ['spatz.services', 'spatz.controllers', 'spatz.directives']

angular.module('spatz.services', [])
.factory('Schedule', ($http) ->
  Schedule = {}

  Schedule.fetch = (callback) ->
    $http.get('/schedule', params: {})
      .success (data) ->
        callback(data)

  Schedule
)

.factory('Todos', ($http) ->
  Todos = {}

  Todos.fetch = (callback) ->
    $http.get('/todos', params: {})
      .success (data) ->
        callback(data)

  Todos
)    

angular.module('spatz.controllers', ['spatz.services']).controller 'ScheduleController'

angular.module('spatz.controllers', ['spatz.services']).controller 'TasksController'

angular.module('spatz.controllers', ['spatz.services']).controller 'TodosController'
