class Ǵroup < ActiveRecord::Base
	attr_accessible :group_name
	has_many :pupils
	belongs_to :teacher

	has_many :subjects, :through => :lessons
end