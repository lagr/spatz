class Task < ActiveRecord::Base
	attr_accessible :description, :done, :hints
	belongs_to :concrete_lesson
end