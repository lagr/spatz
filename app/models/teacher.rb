class Teacher < Person
	has_one :group
	has_many :subjects
end