class Todo < ActiveRecord::Base
	attr_accessible :type, :title, :description, :deadline
end