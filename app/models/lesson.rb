class Lesson < ActiveRecord::Base
	belongs_to :subject
	belongs_to :group

	has_many :timeslots, :through => :concrete_lessons
end