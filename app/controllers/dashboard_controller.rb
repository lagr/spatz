class DashboardController < ApplicationController

	def show
	end


  def schedule
  	@schedule = []

  	respond_to do |format|
  		format.json {render json: @schedule}
  	end
  end

  def tasks
  end

  def todos
  end

  def update_tasks
  end

  def update_todos
  end
  
end
