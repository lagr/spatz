class ApplicationController < ActionController::Base
  protect_from_forgery

  layout 'layout'

  before_filter :init_gon

  def init_gon
    gon.time = Time.now
  end

end
