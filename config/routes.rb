Angschool::Application.routes.draw do
  root :to => "dashboard#show"
 
  match "tasks", :to => "dashboard#tasks"
  match "todos", :to => "dashboard#todos"

end
