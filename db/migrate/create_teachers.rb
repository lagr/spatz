class CreateTeachers < ActiveRecord::Migration
	def change
		create_table :teachers do |t|
			t.string :name
			t.references :group

			t.timestamps
		end
	end
end