class CreateTodos < ActiveRecord::Migration
	def change
		create_table :todos do |t|
			t.string :type
			t.string :title
			t.text :description
			t.datetime :deadline

			t.timestamps
		end
	end
end