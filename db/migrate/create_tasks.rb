class CreateTasks < ActiveRecord::Migration
	def change
		create_table :tasks do |t|
			t.text :description
			t.text :hints
			t.boolean :done

			t.timestamps
		end
	end
end